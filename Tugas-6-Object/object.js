// // soal no 1 awal
console.log("\n==============Soal No. 1 ===========");

function arrayToObject(arr) {
  if (arr.length > 0) {
    let obj = [];
    arr.map((item, i) => {
      let now = new Date();
      let thisYear = now.getFullYear();

      if (item[3]) {
        item[3] < thisYear
          ? (item[3] = thisYear - item[3])
          : (item[3] = "Invalid birth year");
      } else {
        item[3] = "Invalid birth year";
      }

      let name = i + 1 + ". " + item[0] + " " + item[1];
      let detail = {
        firstName: item[0],
        lastName: item[1],
        gender: item[2],
        age: item[3],
      };
      obj[name] = detail;
    });
    console.log(obj);
    return obj;
  } else {
    let obj = " ";
    console.log(obj);
    return obj;
  }
}

var input = [
  ["Abduh", "Muhamad", "male", "1992"],
  ["Ahmad", "Taufik", "male", "1985"],
];

arrayToObject(input);

var people = [
  ["Bruce", "Banner", "male", 1975],
  ["Natasha", "Romanoff", "female"],
];

arrayToObject(people);

var people2 = [
  ["Tony", "Stark", "male", 1980],
  ["Pepper", "Pots", "female", 2023],
];
arrayToObject(people2); 


// soal no 1 akhir
// =========================
console.log("\n==============Soal No. 2 ===========");

// soal no 2 awal
let shoppingList = [
    {item: 'Sepatu Stacattu', price: 1500000},
    {item: 'Baju Zoro', price:500000},
    {item: 'Baju H&N', price: 250000},
    {item: 'Sweater Uniklooh', price:175000},
    {item: 'Casing Handphone', price: 50000}
]
function shoppingTime(memberId, money) {
    const lowestPrice = Math.min(...shoppingList.map(item => parseInt(item.price)));

    if(!memberId) {
        return 'Mohon maaf, toko X hanya berlaku untuk member saja';
    }
    else {
        if(money>lowestPrice) {
            let obj = {}
            let buy = [];
            obj['memberId'] = memberId;
            obj['money'] = money;
            shoppingList.forEach(item => {
                if(money>=item.price) {
                    buy.push(item.item)
                    money-=item.price;
                }
                else {
                    return "Mohon maaf, uang tidak cukup";
                }
            })
            obj['listPurchased']=buy;
            obj['changeMoney']=money;
            return obj;
        }
        else {
            return "Mohon maaf, uang tidak cukup";
        }
    }
  }
   
// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja


console.log('\n==============Soal No. 3 (Naik Angkot)===========');
function naikAngkot(arrPenumpang) {
rute = ['A', 'B', 'C', 'D', 'E', 'F'];
let output = [];
arrPenumpang.forEach(item => {
    let obj = {};
    let from = rute.indexOf(item[1])+1
    let to = rute.indexOf(item[2])+1
    let tarif = (to-from) * 2000;
    obj['penumpang']=item[0];
    obj['naikDari']=item[1];
    obj['tujuan']=item[2];
    obj['bayar']=tarif;
    output.push(obj);
})
return output;
}

//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]

console.log(naikAngkot([])); //[]



