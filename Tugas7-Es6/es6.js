var batas =  "<=========>"

console.log(batas+"tugas 1"+batas);

const golden = () => {
  console.log("this is golden!!");
};

golden();
console.log(batas + "tugas 2" + batas);

const newFunction = (firstName, lastName) => {
  return {
    firstName,
    lastName,
    fullName() {
      console.log(`${firstName} ${lastName}`);
    },
  };
};

//Driver Code
newFunction("William", "Imoh").fullName();
console.log(newFunction("Wiltesliam", "Imoh").firstName); 

console.log(batas + "tugas 3" + batas);

const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!",
};

const { firstName, lastName, destination, occupation, spell } = newObject;

// Driver code
console.log(firstName, lastName, destination, occupation);

console.log(batas + "tugas 4" + batas);

const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];

let combined = [...west, ...east];
console.log(combined);

console.log(batas + "tugas 5" + batas);
const planet = "earth";
const view = "glass";
var before = `Lorem ${view} dolor sit amet consectetur adipiscing elit, ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`;
console.log(before);











