import * as React from 'react';
import {Button, View, Text} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import LoginScreen from './LoginScreen';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import SkillScreen from './SkillScreen';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import AboutScreen from './AboutScreen';
import ProjectScreen from './ProjectScreen';
import AddScreen from './AddScreen';
import {createDrawerNavigator} from '@react-navigation/drawer';


const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();
const Drawer = createDrawerNavigator();

;




function HomeScreen({navigation}) {
  return (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <Text>Home Screen</Text>
      <Button
        title="Go to Login Screen"
        onPress={() => navigation.navigate('LoginSc')}
      />

      <Button
        color="red"
        title="Go to Skill"
        onPress={() => navigation.navigate('SkillSc')}
      />
    </View>
  );
}

function LoginSc({route, navigation}) {
  return (
    <View>        
        <LoginScreen/>
     </View>
  );
}
function SkillSC({route, navigation}) {
  return (


    <Tab.Navigator
      initialRouteName="Feed"
      tabBarOptions={{
        activeTintColor: '#42f44b',
      }}>
      <Tab.Screen
        name="HomeStack"
        component={SkillScreen}
        options={{
          tabBarLabel: 'Skill Screen',
          tabBarIcon: ({color, size}) => (
            <MaterialCommunityIcons name="home" color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="Abosut"
        component={AboutScreen}
        options={{
          tabBarLabel: 'Tantang',
          tabBarIcon: ({color, size}) => (
            <MaterialCommunityIcons name="about" color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="Project"
        component={ProjectScreen}
        options={{
          tabBarLabel: 'Project',
          tabBarIcon: ({color, size}) => (
            <MaterialCommunityIcons name="settings" color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="Add Screen"
        component={AddScreen}
        options={{
          tabBarLabel: 'AddScreen',
          tabBarIcon: ({color, size}) => (
            <MaterialCommunityIcons name="settings" color={color} size={size} />
          ),
        }}
      />
    </Tab.Navigator>
    // <View>
    //   <SkillScreen/>
    // </View>
  );
}
function HomeStack() {
  return (
    <Stack.Navigator
      initialRouteName="Skill"
      screenOptions={{
        headerStyle: {backgroundColor: '#42f44b'},
        headerTintColor: '#fff',
        headerTitleStyle: {fontWeight: 'bold'},
      }}>
      <Stack.Screen
        name="Skill"
        component={SkillScreen}
        options={{title: 'Skill'}}
      />
    </Stack.Navigator>
  );
}

function SettingsStack() {
  return (
    <Stack.Navigator
      initialRouteName="Settings"
      screenOptions={{
        headerStyle: {backgroundColor: '#42f44b'},
        headerTintColor: '#fff',
        headerTitleStyle: {fontWeight: 'bold'},
      }}>
      <Stack.Screen
        name="Settings"
        component={SettingsScreen}
        options={{title: 'Setting Page'}}
      />
      <Stack.Screen
        name="About"
        component={AboutScreen}
        options={{title: 'Details Page'}}
      />
      <Stack.Screen
        name="Profile"
        component={ProfileScreen}
        options={{title: 'Profile Page'}}
      />
    </Stack.Navigator>
  );
}


function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen name="LoginSc" component={LoginSc} />
        <Stack.Screen name="SkillSc" component={SkillSC} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;
