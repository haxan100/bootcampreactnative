import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  Image,
  TouchableOpacity,
  Dimensions,
  TextInput,
  Button,
} from 'react-native';

// import { Card } from 'react-native-screens'; // 0.18.5

import {Card, Icon} from 'react-native-elements';

import data from './data.json';

const DEVICE = Dimensions.get('window');
// console.log(DEVICE)

export default class HomeScreen extends React.Component {
  constructor(props) {
    var datadariLogin = props.route.params.key.nama;
    // var datadariLogin = 'props.route.params.key.nama';

    super(props);
    this.state = {
      searchText: '',
      totalPrice: 0,
      nama: datadariLogin,
      data: data.produk,
    };
    var nama = datadariLogin;
    // console.log(this.state.data);
  }

  currencyFormat(num) {
    return 'Rp ' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
  }
  simpanHarga(harga,stok) {
    console.log(harga,stok)
    if(stok<=0){
      alert('maaf, Stok Habis!')
    }else{
      var  price = this.state.totalPrice + parseInt(harga);
       
       this.setState({totalPrice: price});

    }
    // console.log(this.state.totalPrice);
    
  }

  updatePrice() {
    alert('oke');
    return false;
    price = this.state.totalPrice + parseInt(price);

    this.setState({totalPrice: price});
    console.log(this.state);
  }

  renderItem = ({item}) => console.log('yayaya');

  ubah(angka, prefix) {
    var number_string = angka.replace(/[^,\d]/g, '').toString(),
      split = number_string.split(','),
      sisa = split[0].length % 3,
      rupiah = split[0].substr(0, sisa),
      ribuan = split[0].substr(sisa).match(/\d{3}/gi);

    // tambahkan titik jika yang di input sudah menjadi angka ribuan
    if (ribuan) {
      var separator = sisa ? '.' : '';
      rupiah += separator + ribuan.join('.');
    }

    rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
    return prefix == undefined ? rupiah : rupiah ? 'Rp. ' + rupiah : '';
  }

  render() {
    return (
      <View style={styles.container}>
        <View
          style={{
            minHeight: 50,
            width: DEVICE.width * 0.88 + 20,
            marginVertical: 8,
          }}>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <Text>
              Hai,
              {/* //? #Soal 1 Tambahan, Simpan userName yang dikirim dari halaman Login pada komponen Text di bawah ini */}
              <Text style={styles.headerText}>{this.state.nama}</Text>
            </Text>

            {/* //? #Soal Bonus, simpan Total Harga dan state.totalPrice di komponen Text di bawah ini */}
            <Text style={{textAlign: 'right'}}>
              Total Harga{'\n'}
              <Text style={styles.headerText}>
                {this.currencyFormat(this.state.totalPrice)}
              </Text>
            </Text>
          </View>
          <View></View>
          <TextInput
            style={{backgroundColor: 'white', marginTop: 8}}
            placeholder="Cari barang.."
            onChangeText={(searchText) => this.setState({searchText})}
          />
        </View>
        <FlatList
          data={this.state.data}
          style={styles.container}
          numColumns={2}
          contentContainerStyle={styles.list}
          renderItem={({item}) => (
            <View>
              <Card containerStyle={[styles.card, {height: 280}]}>
                <Card.Title>{item.nama}</Card.Title>
                <Card.Divider />
                <Image
                  style={styles.tinyLogo}
                  source={{
                    uri: `${item.gambaruri}`,
                  }}
                />
                <Text style={styles.name}>{item.kategori}</Text>
                <Text style={styles.name}>{item.vendor}</Text>
                <Text style={styles.itemPrice}>
                  {this.ubah(item.harga, 'Rp.')}
                </Text>
                <Text style={styles.itemStock}>
                  Sisa stok: {item.stock - 1}
                </Text>
                <Button
                  title="BELI"
                  color="blue"
                  onPress={() =>
                    this.simpanHarga(item.harga,item.stock)
                  }
                />
              </Card>
            </View>
          )}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    );
  }
}


class ListItem extends React.Component {
  currencyFormat(num) {
    return 'Rp ' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
  }

  //? #Soal No 3 (15 poin)
  //? Buatlah styling komponen ListItem, agar dapat tampil dengan baik di device

  render() {
    const data = this.props.data.item;
    return (
      <View style={styles.itemContainer}>
        <Image
          source={{uri: data.gambaruri}}
          style={styles.itemImage}
          resizeMode="contain"
        />
        <Text numberOfLines={2} ellipsizeMode="tail" style={styles.itemName}>
          {data.nama}
        </Text>
        <Text style={styles.itemPrice}>
          {this.currencyFormat(Number(data.harga))}
        </Text>
        <Text style={styles.itemStock}>Sisa stok: {data.stock - 1}</Text>
        <Button title="BELI" color="blue" onPress={this.props.updatePrice} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
  },
  headerText: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  card: {
    width: DEVICE.width / 2.3,
    // width: 155,
    margin: 10,
  },
  //? Lanjutkan styling di sini
  itemContainer: {
    width: 144,
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: '#FFFFFF',
    marginHorizontal: 5,
    marginVertical: 5,
    padding: 5,
  },
  itemImage: {},
  itemName: {},
  itemPrice: {
    fontSize:16,
    fontWeight:"400",
    color:'skyblue'
  },
  itemStock: {},
  itemButton: {},
  buttonText: {},
  tinyLogo: {
    width: 50,
    height: 50,
  },
});
