import React, {Component} from 'react';
import {Text, View, StyleSheet, Image, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';




export default class VideoData extends Component {
    render() {
      let video = this.props.video.item;

    //   alert('video.id')
    return (
      <View style={styles.container}>
        <View>
          <Image
            source={{uri: video.snippet.thumbnails.medium.url}}
            style={styles.thumnail}
          />
          <View style={styles.descContainer}>
            <Image
              source={{uri: 'https://randomuser.me/api/portraits/men/0.jpg'}}
              style={styles.iconThum}
            />
            <View style={styles.vidDetail}>
                <Text style={styles.textTitle}> {video.snippet.title}</Text>                  
                <Text style={styles.videoStats}>{video.snippet.channelTitle + " · " + nFormatter(video.statistics.viewCount, 1) + " · 3 months ago "}</Text>
                  


            </View>
                <TouchableOpacity>
                    <Icon name="more-vert" size={20} color="#999999"/>
                </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}
function nFormatter(num, digits) {
  var si = [
    {value: 1, symbol: ''},
    {value: 1e3, symbol: 'k'},
    {value: 1e6, symbol: 'M'},
    {value: 1e9, symbol: 'G'},
    {value: 1e12, symbol: 'T'},
    {value: 1e15, symbol: 'P'},
    {value: 1e18, symbol: 'E'},
  ];
  var rx = /\.0+$|(\.[0-9]*[1-9])0+$/;
  var i;
  for (i = si.length - 1; i > 0; i--) {
    if (num >= si[i].value) {
      break;
    }
  }
  return (
    (num / si[i].value).toFixed(digits).replace(rx, '$1') +
    si[i].symbol +
    ' views'
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 15,
  },
  thumnail: {
    height: 200,
  },
  descContainer: {
    flexDirection: 'row',
    paddingTop: 15,
  },
  iconThum: {
    height: 50,
    width: 50,
    borderRadius: 25,
  },
  vidDetail: {
    paddingHorizontal: 8,
    flex: 1,
  },
  textTitle: {
    fontSize: 15,
    color: '#4c4c4c',
  },
  VidStyle: {
    fontSize: 15,
    color: '#4c4c4c',
  },

  videoDetails: {
    paddingHorizontal: 15,
    flex: 1,
  },
  videoStats: {
    fontSize: 15,
    paddingTop: 3,
  },
});