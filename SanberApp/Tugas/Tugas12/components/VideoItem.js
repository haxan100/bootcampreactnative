import React, { Component } from 'react'
import { Text, View,StyleSheet, Image, TouchableOpacity,FlatList } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons';
import VideoData from './VideoData';
import data from '../data.json';

export default class VideoItem extends Component {
  render() {
    var d = data;
      // console.log({d})
      return (
          <View style={styles.container}>
            <View style={styles.navbar}>
              <Image
                style={styles.imagesLogo}
                source={require('../images/logo.png')}
              />
              <View style={styles.rightNav}>
                <Icon style={styles.navIcon} name="search" size={25} />
                <TouchableOpacity onPress={() => alert('d')}>
                  <Icon name="account-circle" size={25} />
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.body}>
            {/* <VideoData video={data.items[0]} /> */}
            <FlatList 
              data={data.items}
              keyExtractor= {(item)=>item.id}
              renderItem={(vid)=><VideoData video={vid} />}
              ItemSeparatorComponent={()=><View style={{height:0.5,backgroundColor:'#E5E5E5'}}/>}
            />


            </View>
            <View style={styles.tabBar}>
              <TouchableOpacity style={styles.tabItem}>
                <Icon name="home" size={25} />
                <Text style={styles.textTitle}>Home</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.tabItem}>
                <Icon name="whatshot" size={25} />
                <Text style={styles.textTitle}>Tranding</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.tabItem}>
                <Icon name="subscriptions" size={25} />
                <Text style={styles.textTitle}>Subcrib</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.tabItem}>
                <Icon name="folder" size={25} />
                <Text style={styles.textTitle}>Library</Text>
              </TouchableOpacity>
            </View>
          </View>
        );
    }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  navIcon: {
    marginRight: 34,
  },
  navbar: {
    paddingTop: 15,
    height: 65,
    backgroundColor: 'white',
    elevation: 3,
    paddingHorizontal: 15,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  rightNav: {
    flexDirection: 'row',
  },
  imagesLogo: {
    width: 98,
    height: 22,
  },
  tabBar: {
    // backgroundColor: 'red',
    height: 60,
    borderTopColor: '#e5e5e5',
    borderTopWidth: 0.5,
    flexDirection:'row',
    justifyContent:'space-around',

  },
  body: {
    flex: 1,
  },
  tabItem:{
    alignContent:'center',
    alignItems:'center'
  },
  textTitle:{
    fontSize:11,
    color:'#3c3c3c',
    paddingTop:3
  }
});