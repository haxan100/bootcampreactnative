import React from 'react';
import {
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import gambar from './assets/logo.png';

const RegisterScreen = () => {
  return (
    <View>
      <View style={styles.containerImage}>
        <Image
          source={require('./assets/logo.png')}
          style={{width: 375, height: 102, marginTop: 63}}
        />
        <Text
          style={{
            fontSize: 24,
            fontWeight: '400',
            paddingTop: 43,
            color: '#003366',
          }}>
          Register
        </Text>
      </View>

      <View style={styles.ContainerIsi}>
        <Text
          style={{
            fontWeight: '400',
            fontSize: 16,
            color: '#003366',
            paddingBottom: 4,
          }}>
          Username
        </Text>
        <TextInput
          style={{borderColor: 'gray', borderWidth: 1, marginBottom: 16}}
        />

        <Text
          style={{
            fontWeight: '400',
            fontSize: 16,
            color: '#003366',
            paddingBottom: 4,
          }}>
          Email
        </Text>
        <TextInput
          style={{borderColor: 'gray', borderWidth: 1, marginBottom: 16}}
        />

        <Text
          style={{
            fontWeight: '400',
            fontSize: 16,
            color: '#003366',
            paddingBottom: 4,
          }}>
          Password
        </Text>
        <TextInput
          style={{borderColor: 'gray', borderWidth: 1, marginBottom: 16}}
        />

        <Text
          style={{
            fontWeight: '400',
            fontSize: 16,
            color: '#003366',
            paddingBottom: 4,
          }}>
          Ulangi Password
        </Text>
        <TextInput style={{borderColor: 'gray', borderWidth: 1}} />
      </View>
      <View style={styles.ContainerButton}>
        <TouchableOpacity
          style={{
            alignItems: 'center',
            backgroundColor: '#003366',
            //   padding: 10,
            width: 140,
            height: 40,
            borderRadius: 16,
          }}>
          <Text style={{fontSize: 24, color: '#ffffff'}}>Daftar</Text>
        </TouchableOpacity>
        <Text
          style={{
            color: '#3EC6FF',
            fontSize: 24,
            paddingTop: 16,
            paddingBottom: 16,
          }}>
          Atau
        </Text>

        <TouchableOpacity
          style={{
            alignItems: 'center',
            backgroundColor: '#3EC6FF',
            //   padding: 10,
            width: 140,
            height: 40,
            borderRadius: 16,
          }}>
          <Text style={{fontSize: 24, color: '#ffffff'}}>Masuk</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default RegisterScreen;

const styles = StyleSheet.create({
  containerImage: {
    flex: 1,
    justifyContent: 'center',
    flexDirection: 'column',
    alignItems: 'center',
  },
  ContainerIsi: {
    marginTop: 40,
    flex: 1,
    paddingHorizontal: 41,
  },
  ContainerButton: {
    paddingTop: 32,
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
