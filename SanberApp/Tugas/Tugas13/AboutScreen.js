import React from 'react'
import { Image, StyleSheet, Text, View } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';

const AboutScreen = () => {
    return (
      <View style={styles.containerAwal}>
        <View style={styles.containerText}>
          <Text style={styles.textTentang}> Tentang Saya</Text>

          <Image
            style={styles.Logo}
            source={{
              uri: 'http://heyiamhasan.com/assets/img/about/1.jpg',
            }}
          />
          <Text style={styles.textNama}> Abdul Hasan</Text>
          <Text style={styles.textRND}> React Native Developer</Text>

          <View style={styles.containerPorto}>
            <Text style={styles.textPorto}>Portofolio</Text>
            <View style={styles.garis} />

            <View style={styles.containerIcon}>
              <View style={styles.iconNama}>
                <Icon name="gitlab" size={45} color="#3ec6ff" />
                <Text style={{color: '#003366'}}>@Haxan100</Text>
              </View>

              <View style={styles.iconNama}>
                <Icon name="github" size={45} color="#3ec6ff" />
                <Text>@Haxan100</Text>
              </View>
            </View>
          </View>

          <View style={styles.containerHubSaya}>
            <Text style={styles.textPorto}>Hubungi Saya</Text>
            <View style={styles.garis} />

            <View style={styles.containerIconHubSaya}>
              <View style={styles.iconNamaHub}>
                <Icon
                  style={styles.iconHub}
                  name="facebook-square"
                  size={45}
                  color="#3ec6ff"
                />
                <Text style={{color: '#003366', marginLeft: -40}}>
                  abdul.gostand
                </Text>
              </View>

              <View style={styles.iconNamaHub}>
                <Icon
                  style={styles.iconHub}
                  name="instagram"
                  size={45}
                  color="#3ec6ff"
                />
                <Text style={styles.textHub}>@heyiamhasan</Text>
              </View>

              <View style={styles.iconNamaHub}>
                <Icon
                  style={styles.iconHub}
                  name="twitter"
                  size={45}
                  color="#3ec6ff"
                />
                <Text style={styles.textHub}>@Haxan100</Text>
              </View>
            </View>
          </View>
        </View>
      </View>
    );
}

export default AboutScreen

const styles = StyleSheet.create({
  containerAwal: {
    flex: 1,
    flexDirection: 'column',
  },
  containerText: {
    flex: 1,
    alignItems: 'center',
    paddingTop: 34,
  },
  textTentang: {
    fontSize: 36,
    fontWeight: '700',
    color: '#003366',
  },
  Logo: {
    height: 150,
    width: 150,
    borderRadius:150/2
  },
  textNama: {
    fontSize: 24,
    color: '#003366',
  },
  textRND: {
    fontSize: 16,
    color: '#3ec6ff',
  },
  containerPorto: {
    //   flex:1,
    // justifyContent:'space-between',
    // flexDirection:'column',
    width: 329,
    height: 140,
    borderRadius: 16,
    backgroundColor: '#efefef',
    // paddingLeft: 20,
  },
  textPorto: {
    paddingTop: 10,
    marginLeft: 13,
    fontWeight: '400',
    fontSize: 18,
    color: '#003366',
  },
  garis: {
    paddingTop: 8,
    borderBottomColor: 'black',
    borderBottomWidth: 1,
    marginLeft: 20,
    marginRight: 20,
  },
  containerIcon: {
    height: 60,
    borderTopColor: '#e5e5e5',
    borderTopWidth: 0.5,
    flexDirection: 'row',
    justifyContent: 'space-around',
    // paddingTop: 12,
    // // flex: 1,
    // flexDirection: 'row',
    // justifyContent: 'space-around',

    // alignContent: 'center',
    // alignItems: 'center',

    // height: 60,
    // borderTopColor: '#e5e5e5',
    // borderTopWidth: 0.5,
    // flexDirection: 'row',
    // justifyContent: 'space-around',
  },
  iconNama: {
    alignContent: 'center',
    alignItems: 'center',
    fontSize: 11,
    color: '#3c3c3c',
    paddingTop: 3,
  },
  containerHubSaya: {
    marginTop: 9,
    paddingTop: 7,
    width: 329,
    height: 280,
    borderRadius: 16,
    backgroundColor: '#efefef',
  },
  containerIconHubSaya: {
    height: 60,
    borderTopColor: '#e5e5e5',
    borderTopWidth: 0.5,
  },
  iconNamaHub: {
    //   flex:1,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignContent: 'center',
    alignItems: 'center',
    // fontSize: 15,
    color: '#3c3c3c',
    paddingTop: 18,
  },
  textHub: {
    marginLeft: -40,
    color:'#003366'
  },
  iconHub: {
    marginRight: -40,
  },
});
