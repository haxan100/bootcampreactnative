// tugas 3 lopping awal
var judulPertama = "LOOPING PERTAMA";
var judulKedua = "LOOPING KEDUA";
function enter() {
  console.log("\n");
}
console.log(judulPertama);
var j = 2;
while (j < 21 && j > 1) {
  console.log(j + " - I Love Coding");
  j += 2;
}
enter();

// //Looping Kedua
console.log(judulKedua);
var i = 20;
while (i < 21 && i > 1) {
  console.log(i + " - I Will Become a Mobile Developer");
  i -= 2;
}
enter();
// tugas 3 lopping akhir
// ==========================================================
// tugas 3 for lop awal

console.log("SOAL NO 2");
for (let index = 1; index <= 20; index++) {
  if (index % 3 == 0 && index % 2 != 0) {
    console.log(index + " - I love coding");
  } else if (index % 2 == 0) {
    console.log(index + " - Berkualitas");
  } else if (index % 2 != 0) {
    console.log(index + " - Santai");
  }
}
// tugas 3 for lop akhir
// =============================================
// tugas 3 persegi panjang awal
console.log("SOAL NO 3");
var strout = "";
for (let i = 0; i < 4; i++) {
  strout = "";
  for (let j = 0; j < 8; j++) {
    strout = strout + "#";
  }
  console.log(strout);
}
// tugas 3 persegi panjang akhir
// =============================================
// tugas membuat tangga awal
console.log("");
console.log("SOAL NO 4");
var strout2 = "";
for (let i = 1; i < 8; i++) {
  strout2 = "";
  for (let j = 0; j < i; j++) {
    strout2 = strout2 + "#";
  }
  console.log(strout2);
}
// tugas membuat tangga akhir
// =============================================
// tugas  membuat papan catur awal
console.log("SOAL NO 5");
var strout3 = "";
for (let i = 0; i < 8; i++) {
  strout3 = "";
  if (i % 2 == 0) {
    //Baris Genap
    for (let j = 0; j < 4; j++) {
      strout3 = strout3 + " #";
    }
  } else if (i % 2 != 0) {
    for (let j = 0; j < 4; j++) {
      strout3 = strout3 + "# ";
    }
  }
  console.log(strout3);
}

// tugas  membuat papan catur akhir