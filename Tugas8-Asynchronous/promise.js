function readBooksPromise(time, book) {
  console.log(`saya membaca buku ${book.name}`);
  return new Promise( function (resolve,reject) { 
      setTimeout(() => {
          let sisaWaktu = time -  book.timeSpent;
          if(sisaWaktu>=0){
              console.log(`saya sudah selesai membaca buku ${book.name}, sisa waktu saya ${sisaWaktu}`)
              resolve(sisaWaktu)
          }else{
              console.log(`saya sudah tidak punya waktu untuk membaca buku ${book.name}`)
              reject(sisaWaktu)
          }
          
      }, book.timeSpent);
   });
}

  module.exports = readBooksPromise;